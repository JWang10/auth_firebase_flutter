import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:login_app/controllers/loginController.dart';

import 'login_page.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Get.lazyPut<LoginController>(() => LoginController());
  // await () => {Get.lazyPut<LoginController>(() => LoginController());};
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(),
    );
  }
}
