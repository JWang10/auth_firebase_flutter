import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:login_app/controllers/loginController.dart';

class LoginPage extends StatelessWidget {
  TextEditingController emailController = TextEditingController();
  TextEditingController pwdController = TextEditingController();

  var loginController = Get.find<LoginController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Stack(
          children: [
            Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xFF73AEF5),
                    Color(0xFF61A4F1),
                    Color(0xFF478de0),
                    Color(0xFF398ae5),
                  ],
                  stops: [0.1, 0.4, 0.7, 0.9],
                ),
              ),
            ),
            Container(
              height: double.infinity,
              child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 80.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Sign in",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _buildEmailTF(),
                        SizedBox(height: 30.0),
                        _buildPWDTF(),
                      ],
                    ),
                    SizedBox(height: 20.0),
                    _buildForgotPWDBtn(context),
                    _buildRememberMeCheckBox(),
                    _buildLoginBtn(context),
                    _buildSignInWith(),
                    _buildSocialBtnRow(context),
                    _buildSignUpBtn(context),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildEmailTF() {
    return Container(
      padding: EdgeInsets.only(left: 10),
      decoration: BoxDecoration(
        color: Color(0xFF73AEF5),
        borderRadius: BorderRadius.circular(30),
        boxShadow: [
          BoxShadow(color: Colors.black45, offset: Offset(3, 4), blurRadius: 5),
        ],
      ),
      child: TextField(
        controller: emailController,
        keyboardType: TextInputType.emailAddress,
        style: TextStyle(color: Colors.white),
        decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.only(top: 14.0),
          prefixIcon: Icon(
            Icons.email,
            color: Colors.white,
          ),
          hintText: "Enter your email",
          hintStyle: TextStyle(color: Colors.white60),
        ),
      ),
    );
  }

  Widget _buildPWDTF() {
    return Container(
      padding: EdgeInsets.only(left: 10),
      decoration: BoxDecoration(
        color: Color(0xFF73AEF5),
        borderRadius: BorderRadius.circular(30),
        boxShadow: [
          BoxShadow(color: Colors.black45, offset: Offset(3, 4), blurRadius: 5),
        ],
      ),
      child: TextField(
        obscureText: true,
        controller: pwdController,
        style: TextStyle(color: Colors.white),
        decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.only(top: 14.0),
          prefixIcon: Icon(
            Icons.lock,
            color: Colors.white,
          ),
          hintText: "Enter your password",
          hintStyle: TextStyle(color: Colors.white60),
        ),
      ),
    );
  }

  Widget _buildForgotPWDBtn(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      child: InkWell(
        onTap: () {
          // Get.snackbar("Forgot Password", "try again"); // desktop was not work
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text("Forgot Password"),
              duration: Duration(milliseconds: 250),
            ),
          );
        },
        child: Text(
          "Forgot Password?",
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget _buildRememberMeCheckBox() {
    return Container(
      child: Row(
        children: [
          Theme(
            data: ThemeData(unselectedWidgetColor: Colors.white),
            child: Obx(() => Checkbox(
                  value: loginController.isRememberMe,
                  checkColor: Colors.green,
                  activeColor: Colors.white,
                  onChanged: (bool? value) {
                    loginController.isRememberMe = value!;
                  },
                )),
          ),
          Text(
            "Remember Me",
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );
  }
}

Widget _buildSignUpBtn(BuildContext context) {
  return GestureDetector(
    onTap: () {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text("Sign Up"),
          duration: Duration(milliseconds: 250),
        ),
      );
    },
    child: RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: "Don\'t have an Account? ",
            style: TextStyle(
                color: Colors.white, fontSize: 18, fontWeight: FontWeight.w400),
          ),
          TextSpan(
            text: "Sign Up",
            style: TextStyle(
                color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    ),
  );
}

Widget _buildLoginBtn(BuildContext context) {
  return Container(
    padding: EdgeInsets.symmetric(vertical: 25.0),
    width: double.infinity,
    child: ElevatedButton(
      onPressed: () {
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: Text("Login"),
                content: Text("Login!!"),
              );
            });
        // ScaffoldMessenger.of(context).showSnackBar(
        //   SnackBar(
        //     content: Text("LOGIN"),
        //     duration: Duration(milliseconds: 250),
        //   ),
        // );
      },
      child: Text(
        "LOGIN",
        style: TextStyle(
            color: Color(0xff527daa),
            fontSize: 20,
            letterSpacing: 1.5,
            fontWeight: FontWeight.bold),
      ),
      style: ElevatedButton.styleFrom(
        elevation: 5.0,
        primary: Colors.white,
        fixedSize: Size(0, 50),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
    ),
  );
}

Widget _buildSignInWith() {
  return Column(
    children: [
      Text(
        "- OR -",
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
      ),
      SizedBox(height: 10),
      Text(
        "Sign in with ",
        style: TextStyle(color: Colors.white),
      ),
    ],
  );
}

Widget _buildSocialBtn({
  required Function() onTap,
  required BuildContext context,
  required AssetImage image,
}) {
  return GestureDetector(
    onTap: onTap,
    child: Container(
      height: 60.0,
      width: 60.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Colors.black26, offset: Offset(0, 2), blurRadius: 6.0),
        ],
        image: DecorationImage(
          image: image,
        ),
      ),
    ),
  );
}

Widget _buildSocialBtnRow(BuildContext context) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: 30.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _buildSocialBtn(
          onTap: () {
            // Get.defaultDialog(
            //     title: "Login", middleText: "Login with Facebook");
            print("Login with Facebook");
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text("Login with Facebook"),
                duration: Duration(milliseconds: 250),
              ),
            );
          },
          context: context,
          image: AssetImage("assets/logo/facebook.jpg"),
        ),
        _buildSocialBtn(
          onTap: () {
            // Get.defaultDialog(title: "Login", middleText: "Login with Google");
            print("Login with Google");
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text("Login with Google"),
                duration: Duration(milliseconds: 250),
              ),
            );
          },
          context: context,
          image: AssetImage("assets/logo/google.jpg"),
        ),
      ],
    ),
  );
}
