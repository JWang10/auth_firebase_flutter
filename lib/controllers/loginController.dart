import 'package:get/get.dart';

class LoginController extends GetxController {
  RxBool _isRememberMe = false.obs;
  bool get isRememberMe => _isRememberMe.value;

  set isRememberMe(bool check) => _isRememberMe.value = check;
}
